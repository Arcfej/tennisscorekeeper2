package com.example.android.tennisscorekeeper2;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    int firstPlayerScore = 0;
    int secondPlayerScore = 0;
    int firstPlayerSet = 0;
    int secondPlayerSet = 0;
    boolean firstPlayerAdvantage = false;
    boolean secondPlayerAdvantage = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        displayFirstPlayerSet(firstPlayerSet);
        displaySecondPlayerSet(secondPlayerSet);
        displayFirstPlayerScore(getString(R.string.love));
        displaySecondPlayerScore(getString(R.string.love));
    }

    /**
     * display score for 1st player
     */
    public void displayFirstPlayerScore(String score) {
        TextView scoreView = (TextView) findViewById(R.id.text_first_player_score);
        scoreView.setText(score);
    }

    /**
     * display score for 2nd player
     */
    public void displaySecondPlayerScore(String score) {
        TextView scoreView = (TextView) findViewById(R.id.text_second_player_score);
        scoreView.setText(score);
    }

    /**
     * display set wins for 1st player
     */
    public void displayFirstPlayerSet(int set) {
        TextView setView = (TextView) findViewById(R.id.text_first_player_set_wins);
        setView.setText(String.valueOf(set));
    }

    /**
     * display set wins for 2nd player
     */
    public void displaySecondPlayerSet(int set) {
        TextView setView = (TextView) findViewById(R.id.text_second_player_set_wins);
        setView.setText(String.valueOf(set));
    }

    /**
     * set points for 1st player
     */
    public void pointForFirstPlayer(View view) {
        firstPlayerScore = firstPlayerScore + 15;
        if (firstPlayerScore == 45) {
            firstPlayerScore = 40;
            if (secondPlayerScore == 40) {
                displayFirstPlayerScore(getString(R.string.deuce));
                displaySecondPlayerScore(getString(R.string.deuce));
            } else {
                displayFirstPlayerScore(Integer.toString(firstPlayerScore));
            }
        } else if (firstPlayerScore > 45) {
            /* The "SET WIN" buttons became unnecessary thanks to the following 'if' statement,
            * but I left the buttons in my program because of the project criteria. (There has
            * to be multiple buttons in each column). The "SET WIN" buttons still work.
            * */
            if (secondPlayerScore < 40 || firstPlayerAdvantage) {
                increaseFirstPlayerSetWins(null);
            } else if (secondPlayerAdvantage) {
                secondPlayerAdvantage = false;
                firstPlayerScore = 40;
                displayFirstPlayerScore(getString(R.string.deuce));
                displaySecondPlayerScore(getString(R.string.deuce));
            } else {
                firstPlayerAdvantage = true;
                displayFirstPlayerScore(getString(R.string.advantage));
                firstPlayerScore = 40;
                displaySecondPlayerScore(Integer.toString(secondPlayerScore));
            }
        } else {
            displayFirstPlayerScore(Integer.toString(firstPlayerScore));
        }
    }

    /**
     * set points for 2nd player
     */
    public void pointForSecondPlayer(View view) {
        secondPlayerScore = secondPlayerScore + 15;
        if (secondPlayerScore == 45) {
            secondPlayerScore = 40;
            if (firstPlayerScore == 40) {
                displayFirstPlayerScore(getString(R.string.deuce));
                displaySecondPlayerScore(getString(R.string.deuce));
            } else {
                displaySecondPlayerScore(Integer.toString(secondPlayerScore));
            }
        } else if (secondPlayerScore > 45) {
            /* The "SET WIN" buttons became unnecessary thanks to the following 'if' statement,
            * but I left the buttons in my program because of the project criteria. (There has
            * to be multiple buttons in each column). The "SET WIN" buttons still work.
            * */
            if (firstPlayerScore < 40 || secondPlayerAdvantage) {
                increaseSecondPlayerSetWins(null);
            } else if (firstPlayerAdvantage) {
                firstPlayerAdvantage = false;
                secondPlayerScore = 40;
                displayFirstPlayerScore(getString(R.string.deuce));
                displaySecondPlayerScore(getString(R.string.deuce));
            } else {
                secondPlayerAdvantage = true;
                displaySecondPlayerScore(getString(R.string.advantage));
                secondPlayerScore = 40;
                displayFirstPlayerScore(Integer.toString(firstPlayerScore));
            }
        } else {
            displaySecondPlayerScore(Integer.toString(secondPlayerScore));
        }
    }

    /**
     * increase set wins for 1st player and erase points
     */
    public void increaseFirstPlayerSetWins(View view) {
        firstPlayerSet = firstPlayerSet + 1;
        displayFirstPlayerSet(firstPlayerSet);
        firstPlayerScore = 0;
        displayFirstPlayerScore(getString(R.string.love));
        secondPlayerScore = 0;
        displaySecondPlayerScore(getString(R.string.love));
        firstPlayerAdvantage = false;
        secondPlayerAdvantage = false;
    }

    /**
     * increase set wins for 2nd player and erase points
     */
    public void increaseSecondPlayerSetWins(View view) {
        secondPlayerSet = secondPlayerSet + 1;
        displaySecondPlayerSet(secondPlayerSet);
        firstPlayerScore = 0;
        displayFirstPlayerScore(getString(R.string.love));
        secondPlayerScore = 0;
        displaySecondPlayerScore(getString(R.string.love));
        firstPlayerAdvantage = false;
        secondPlayerAdvantage = false;
    }

    /**
     * reset button
     */
    public void reset(View view) {
        firstPlayerScore = 0;
        secondPlayerScore = 0;
        firstPlayerSet = 0;
        secondPlayerSet = 0;
        firstPlayerAdvantage = false;
        secondPlayerAdvantage = false;
        displayFirstPlayerScore(getString(R.string.love));
        displaySecondPlayerScore(getString(R.string.love));
        displayFirstPlayerSet(firstPlayerSet);
        displaySecondPlayerSet(secondPlayerSet);
    }
}
